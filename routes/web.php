<?php

use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    $user = $request->user();

    // $user->updatePermission(['edit posts']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'role:admin'], function() {
    Route::get('/admin', function() {
        return 'admin panel';
    });
});
